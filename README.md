# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| rainbow color                                    | 1~5%     | Y         | 
| fill color                                       | 1~5%     | Y         | 


---

## How to use(基本功能介紹)

### 1.整體介面
![](https://i.imgur.com/VcwYTTi.png)
    
### 2. ![](https://i.imgur.com/3UstC9o.png) Color Selector顏色選擇器
在點開顏色選擇器之後，會出現下圖的顏色選擇方塊，可以選擇畫筆、直線工具、圓圈工具、三角形工具、長方形工具、文字方塊的顏色。

![](https://i.imgur.com/8ALBrWm.png)
### 3. ![](https://i.imgur.com/H1lnQCW.png) Brush Size(筆觸大小調整滑桿) 
筆觸大小調整滑桿能夠調整畫筆、直線工具、圓圈工具、三角形工具、長方形工具的筆觸粗細。其範圍為0px~100px，預設值為10px。
### 4. ![](https://i.imgur.com/YwLaZyJ.png) Load picture(上傳圖片)
按下上傳圖片後，將選擇的圖片畫至畫布，而畫布將會變得和圖片一樣大，並且無條件初始化為第一步 ( 因此按上一步會是空白而不是上傳圖片前的狀態與畫布大小 ) ，但如果按下reset button畫布則會變回預設大小。
### 5. ![](https://i.imgur.com/eWNfUG8.png) Font and Text Size(字體與字型大小)
字體由介面左上方的選單選擇，字型大小則於其右側輸入。
### 6.![](https://i.imgur.com/JNhchrF.png) Reset button(重置)
在按下重置之後，所有先前步驟都會歸零，變成一張全新、空白的畫布，並且畫布大小變為預設值。

### 7. ![](https://i.imgur.com/r0p3yJu.png) Pen tool(畫筆工具)
在進入canvas之後，最一開始的預設動作即為畫筆，可以直接在畫板上作畫。
在作畫時，游標會變為![](https://i.imgur.com/b5gTq7Z.png)型態。
### 8. ![](https://i.imgur.com/FikdVrF.png) Eraser(橡皮擦)
在按下橡皮擦之後，可以清除所有工具產生的圖案。
在使用橡皮擦的時候，游標會變為 ![](https://i.imgur.com/mJ8jFp3.png) 型態。

### 9. ![](https://i.imgur.com/GVfkAro.png) Line tool(直線工具)
其線條粗細與顏色調整方式與畫筆一樣，皆由滑桿與顏色選擇器調整。在按下直線工具之後，可以產生從**按下滑鼠位置**到**放開滑鼠位置**之間的一條直線。只要滑鼠超出畫板就會視為滑鼠放開並停止該次作畫。
(以下簡稱為mousedown與mouseup)
在使用直線工具時，游標會變為 ![](https://i.imgur.com/au0Q3M3.png) 型態。

### 10. ![](https://i.imgur.com/rOqSzew.png) Circle tool(圓圈工具)
在按下圓圈工具之後，可以產生以mousedown為固定基準點，並以mousedown與mouseup之間距離為直徑的圓圈。預設圖案為空心。
在使用圓圈工具時，游標會變為 ![](https://i.imgur.com/ZY4VmAO.png) 型態。

### 11. ![](https://i.imgur.com/UJl68AM.png) Triangle tool(三角形工具)
在按下三角形工具之後，可以產生以mousedown為三角形上方頂點，mouseup為等腰三角形其中一邊頂點的三角形。預設圖案為空心。
在使用三角形工具時，游標會變為 ![](https://i.imgur.com/AON3Ljg.png) 型態。
### 12. ![](https://i.imgur.com/Rw5cANh.png)Rectangle tool(長方形工具)
在按下長方形工具之後，可以產生以mousedown為上方其中一頂點，mouseup為下方對角頂點的長方形。預設圖案為空心。
在使用長方形工具時，游標會變為 ![](https://i.imgur.com/Tv9licD.png) 型態。
### 13. ![](https://i.imgur.com/2EWbJHc.png) Text box(文字方塊)
在按下文字方塊後，會出現一個文字輸入框，輸入文字後按下enter即可將文字畫於畫布上
在使用文字方塊工具時，游標會變為 ![](https://i.imgur.com/azqCg53.png) 型態。
### 14. ![](https://i.imgur.com/1OCd2Wf.png) Redo(上一步)
按下上一步之後，能將畫面回復至上一個步驟。若已經到達最初畫面則會出現警示：
![](https://i.imgur.com/EGnploe.png)

### 15. ![](https://i.imgur.com/1ti0VnH.png) Undo(下一步)
按下下一步之後，能將畫面重新繪製到畫面上。若已經到達最後畫面則會出現警示：
![](https://i.imgur.com/7OXchd0.png)

### 16. ![](https://i.imgur.com/VZcMRzg.png) Download picture(下載圖片)
按下下載圖片後，網頁會下載檔名為「mycanvas.png」的圖檔至本地端的「下載」資料夾。

## Function description(額外功能介紹)
### 1. ![](https://i.imgur.com/2xnG6he.png) Rainbow Color(彩虹小馬筆刷)

在按下彩虹筆刷之後，畫筆會隨著移動時間變換不同的顏色，具體效果如下所示：
![](https://i.imgur.com/TSwA4EZ.png)
若按下之後使用畫圖案工具，則會在拖曳圖案的過程中變換不同的顏色，但最後圖案只會有一種色彩。
若不想使用彩虹筆刷，只要在按一次圖示即可取消效果。
### 2. ![](https://i.imgur.com/JRpqbHQ.png) Fill Color(實心圖案)
若按下實心圖案，則每一個畫圖案的工具由畫出空心圖案轉變為實心。若不想使用，在按一次即可取消效果。

## Gitlab page link

[https://107062213.gitlab.io/AS_01_WebCanvas/](https://107062213.gitlab.io/AS_01_WebCanvas/)

## Others (Optional)
助教辛苦啦!!!!!!!
