
//window.URL = window.URL || window.webkitURL;
var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
var txtbox = document.getElementById("textbox");
var canvasOffset = getOffset(canvas);
var offsetX = canvasOffset.x;                       
var offsetY = canvasOffset.y;
//flags of functions
var enable_rect = false;
var enable_trian = false;
var enable_circle = false;
var enable_line = false;
var enable_draw = true;
var enable_txt = false;
var enable_fill = false;
var enable_rainbow = false;
var eraser = false;
//save history
let canvashistory = [];
let step = -1;
//draw img
var isdown = false;
//draw pen
var isdraw = false;
//rainbow color
let hue = 0;
var img_color;
canvas.width = window.innerWidth-300;
canvas.height = window.innerHeight - 200;
//最近點擊位置
let mouse = {
    x : 0,
    y : 0,
}
let last = {
    x : 0,
    y : 0,
}
//initial brush
function init_brush()
{
    ctx.lineCap = "round";
    ctx.lineJoin = "round";
    if(enable_rainbow)
    {
        img_color = 'hsl(' + String(hue) + ', 100%, 50%)';
        hue++;
        if(hue>=360)
            hue = 0;
            
    }else
    {
        img_color = document.getElementById("color").value;
    }
    ctx.strokeStyle= img_color;
    ctx.lineWidth=document.getElementById("brushsize").value;
}
//value of brush size
function change_val()
{
    var value = document.getElementById("brushsize").value;
    document.getElementById("valueofbrush").innerHTML = value;
}
//load img
function img()
{
    document.getElementById('upload').onchange = function(e){
        let img = new Image();
        
        img.onload = function(){
            canvas.width = this.width;
            canvas.height = this.height;
            ctx.drawImage(this, 0,0);
            canvashistory = [];
            step = 0;
            cpush();
        }
        img.src = window.URL.createObjectURL(this.files[0]);
    }
}
//change cursor
canvas.addEventListener('mouseenter', (e)=>
{
    if(enable_draw)
        canvas.style.cursor = 'url("./cursor/ca.png") 0 24, default';
    if(eraser)
        canvas.style.cursor = 'url("./cursor/ce.png") 0 24, default';
    if(enable_circle)
        canvas.style.cursor = 'url("./cursor/cc.png") 0 24, default';
    if(enable_trian)
        canvas.style.cursor = 'url("./cursor/ct.png") 0 24, default';
    if(enable_rect)
        canvas.style.cursor = 'url("./cursor/cr.png") 0 24, default';
    if(enable_line)
        canvas.style.cursor = 'url("./cursor/cl.png") 0 24, default';
    if(enable_txt)
        canvas.style.cursor = 'url("./cursor/ctxt.png") 0 24, default';
})

//畫圖案
canvas.onmousedown = function (e)  {
    ctx.save();
    mouse.x = parseInt(e.clientX - offsetX);
    mouse.y = parseInt(e.clientY - offsetY);
    isdown = true;
    if(enable_txt)
        draw_txt();
}
canvas.onmousemove = function (e)  {
    if (!isdown) {
        return;
    }
    mouseX = parseInt(e.clientX - offsetX);
    mouseY = parseInt(e.clientY - offsetY);
    init_brush();
    if(enable_line)
        drawLine(mouseX, mouseY);
    if(enable_rect)
        drawrect(mouseX, mouseY);
    if(enable_trian)
        drawtriangle(mouseX, mouseY);
    if(enable_circle)
        drawcircle(mouseX, mouseY);
}
canvas.onmouseup = function (e)  {
    if (!isdown) {
        return;
    }
    e.preventDefault();
    e.stopPropagation();
    isdown = false;
    if(enable_draw ||enable_trian||enable_rect||enable_line||enable_circle||eraser)
        cpush();
}

function getOffset(element) {
    var xPosition = 0;
    var yPosition = 0;

    while (element) {
        xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
        yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
        element = element.offsetParent;
    }
    return { x: xPosition, y: yPosition };
}
canvas.addEventListener('mousedown', (e)=>
{
    if(enable_draw || eraser)
    {
        isdraw = true;
    }
    if(enable_draw ||enable_trian||enable_rect||enable_line||enable_circle||eraser)
        step++;
    console.log("%d", step);
    [last.x, last.y] = [e.offsetX, e.offsetY];
    
});
canvas.addEventListener('mousemove', draw);
canvas.addEventListener('mouseup', (e) =>
{
    isdraw = false;
});
function draw(e) {
    if (!isdraw) return; // 沒有點擊時不回覆座標
    if(enable_line || enable_circle || enable_rect || enable_trian)
        return;
    if(eraser)
        ctx.globalCompositeOperation = "destination-out";
    else
        ctx.globalCompositeOperation = "source-over";
    init_brush();
    ctx.beginPath();
    //開始
    ctx.moveTo(last.x, last.y);
    //結束
    ctx.lineTo(e.offsetX, e.offsetY);
    //畫圖
    ctx.stroke();
    [last.x, last.y] = [e.offsetX, e.offsetY];
    
}
canvas.addEventListener('mouseleave', (e)=>
{
    isdown = false;
    cpush();
});
//put txt
function textbox()
{
    enable_txt = true;
    enable_line = false;
    enable_draw = false;
    enable_rect = false;
    enable_trian = false;
    enable_circle = false;
    eraser = false;
    
}
function fill_color()
{
    if(enable_fill)
        enable_fill = false;
    else
        enable_fill = true;
}
function draw_txt()
{
    var x = mouse.x;
    var y = mouse.y;
    txtbox.style.left = String(canvas.offsetLeft + x + 24) + "px";
    txtbox.style.top = String(canvas.offsetTop + y - 24) + "px";
    txtbox.style.visibility = "visible";
    
}
document.addEventListener("keydown", function(e){
    var txtsize = document.getElementById("fontsize").value;
    var txtfont = document.getElementById("font").value;
    if(enable_txt)
    {
        if(e.keyCode === 13)
        {
            txtbox.style.visibility = "hidden";
            ctx.fillStyle=document.getElementById("color").value;
            ctx.font = String(txtsize) + "px " + String(txtfont);
            ctx.fillText(txtbox.value, mouse.x + 24, mouse.y);
            console.log("before %d %d", step, canvashistory.length);
            txtbox.value = "";
            step++;
            cpush();
        }
    }
})
function rainbow()
{
    if(enable_rainbow)
        enable_rainbow = false;
    else
        enable_rainbow = true;
}
//draw img
function draw_line()
{
    enable_line = true;
    enable_draw = false;
    enable_rect = false;
    enable_trian = false;
    enable_circle = false;
    eraser = false;
    enable_txt = false;
    txtbox.style.visibility = "hidden";
}

function draw_rectangle()
{
    enable_rect = true;
    enable_trian = false;
    enable_draw = false;
    enable_line = false;
    enable_circle = false;
    eraser = false;
    enable_txt = false;
    enable_txt = false;
    txtbox.style.visibility = "hidden";
}

function draw_triangle()
{
    enable_trian = true;
    enable_rect = false;
    enable_circle = false;
    enable_line = false;
    enable_draw = false;
    eraser = false;
    enable_txt = false;
    txtbox.style.visibility = "hidden";
}

function erase()
{
    eraser = true;
    enable_trian = false;
    enable_rect = false;
    enable_circle = false;
    enable_line = false;
    enable_draw = false;
    enable_txt = false;
    txtbox.style.visibility = "hidden";
}
function pen()
{
    enable_draw = true;
    enable_rect = false;
    enable_trian = false;
    enable_circle = false;
    enable_line = false;
    eraser = false;
    enable_txt = false;
    txtbox.style.visibility = "hidden";
}
function draw_circle()
{
    enable_circle = true;
    enable_draw = false;
    enable_rect = false;
    enable_trian = false;
    enable_line = false;
    eraser = false;
    enable_txt = false;
    txtbox.style.visibility = "hidden";
}

function drawLine(x, y) {
    if(step == 0)
        ctx.clearRect(0,0,canvas.width,canvas.height);
    else
        ctx.putImageData(canvashistory[step - 1], 0, 0);
    ctx.globalCompositeOperation = "source-over";
    ctx.beginPath();
    ctx.moveTo(mouse.x, mouse.y);
    ctx.lineTo(x, y);
    ctx.closePath();
    ctx.stroke();
    ctx.restore();
}

function drawrect(x, y) {
    if(step == 0)
        ctx.clearRect(0,0,canvas.width,canvas.height);
    else
        ctx.putImageData(canvashistory[step - 1], 0, 0);
    ctx.globalCompositeOperation = "source-over";
    ctx.beginPath();
    ctx.moveTo(mouse.x, mouse.y);
    ctx.lineTo(mouse.x,y);
    ctx.lineTo(x,y);
    ctx.lineTo(x,mouse.y);
    ctx.closePath();
    if(enable_fill)
    {
        ctx.fillStyle = img_color;
        ctx.fill();
    }
    ctx.stroke();
}

function drawtriangle(x, y) {
    if(step == 0)
        ctx.clearRect(0,0,canvas.width,canvas.height);
    else
        ctx.putImageData(canvashistory[step - 1], 0, 0);
    ctx.globalCompositeOperation = "source-over";
    ctx.beginPath();
    ctx.moveTo(mouse.x, mouse.y);
    ctx.lineTo(x,y);
    ctx.lineTo(2 * mouse.x - x, y);
    ctx.closePath();
    if(enable_fill)
    {
        ctx.fillStyle = img_color;
        ctx.fill();
    }
    ctx.stroke();
}


function drawcircle(x,y)
{
    if(step == 0)
        ctx.clearRect(0,0,canvas.width,canvas.height);
    else
        ctx.putImageData(canvashistory[step - 1], 0, 0);
    ctx.globalCompositeOperation = "source-over";
    ctx.beginPath();
    var rx = (x-mouse.x)/2;
    var ry = (y-mouse.y)/2;
    var r = Math.sqrt(rx*rx+ry*ry);
    ctx.arc(rx+mouse.x,ry+mouse.y,r,0,Math.PI*2);
    ctx.closePath();
    if(enable_fill)
    {
        ctx.fillStyle = img_color;
        ctx.fill();
    }   
    ctx.stroke();
}

function clearPad(){
    canvashistory = []; 
    step = -1;
    canvas.width = window.innerWidth-300;
    canvas.height = window.innerHeight - 200;
    ctx.clearRect(0,0,canvas.width,canvas.height);
}

function undo()
{
    enable_undo = true;
    txtbox.style.visibility = "hidden";
    if(step >0)
    {
        step--;
        ctx.clearRect(0,0,canvas.width, canvas.height);
        ctx.putImageData(canvashistory[step], 0, 0);
    }
    else if(step == 0)
    {
        step--;
        ctx.clearRect(0,0,canvas.width, canvas.height);
    }
    else{
        ctx.clearRect(0,0,canvas.width, canvas.height);
        alert('沒辦法更往前囉!');
    }
}
function redo() {
    txtbox.style.visibility = "hidden";
    if (step < canvashistory.length - 1) {
      step++;
      ctx.putImageData(canvashistory[step], 0, 0);
    } else {
      alert('已經是最後一步啦!');
    }
}

function cpush()
{
    var imgdata = ctx.getImageData(0,0,canvas.width,canvas.height);
    canvashistory.length = step;
    canvashistory.push(imgdata);
    console.log("cpush %d %d", step, canvashistory.length);
}

let save = document.getElementById("save");

save.onclick = function () {
    let imgUrl = canvas.toDataURL("image/png");
    let saveA = document.createElement("a");
    document.body.appendChild(saveA);
    saveA.href = imgUrl;
    saveA.download = "mycanvas";
    saveA.target = "_blank";
    saveA.click();
};
